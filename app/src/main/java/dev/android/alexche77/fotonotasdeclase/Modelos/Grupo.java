package dev.android.alexche77.fotonotasdeclase.Modelos;

/**
 * Created by alvaro on 9/29/17.
 */

public class Grupo {

    public Grupo() {

    }

    @Override
    public String toString() {
        return nombre;
    }

    private String nombre;
    private String creador;

    public String getIdCreador() {
        return idCreador;
    }

    public void setIdCreador(String idCreador) {
        this.idCreador = idCreador;
    }

    private String idCreador;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }
}
