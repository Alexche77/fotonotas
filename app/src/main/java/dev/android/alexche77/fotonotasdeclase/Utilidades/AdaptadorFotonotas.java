package dev.android.alexche77.fotonotasdeclase.Utilidades;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import dev.android.alexche77.fotonotasdeclase.Modelos.FotoNota;
import dev.android.alexche77.fotonotasdeclase.Modelos.Publicacion;
import dev.android.alexche77.fotonotasdeclase.R;

/**
 * Created by alvaro on 9/30/17.
 */

public class AdaptadorFotonotas extends RecyclerView.Adapter<AdaptadorFotonotas.ViewHolder>{
    private Context context;
    private List<FotoNota> fotoNotas;

    public AdaptadorFotonotas(Context context, List<FotoNota> fotoNotas) {
        this.fotoNotas = fotoNotas;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fotonota_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FotoNota fotoNota = fotoNotas.get(position);

        holder.textViewName.setText(fotoNota.getDescripcion());

        Glide.with(context).load(fotoNota.getUrl()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return fotoNotas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewName;
        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }
}
