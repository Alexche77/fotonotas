package dev.android.alexche77.fotonotasdeclase.Modelos;

/**
 * Created by alvaro on 9/30/17.
 */

public class FotoNota {
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    private String url;
    private String descripcion;

    public String getNombreUnico() {
        return nombreUnico;
    }

    public void setNombreUnico(String nombreUnico) {
        this.nombreUnico = nombreUnico;
    }

    private String nombreUnico;

}