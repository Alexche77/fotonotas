package dev.android.alexche77.fotonotasdeclase.Utilidades;

/**
 * Created by alvaro on 9/29/17.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import dev.android.alexche77.fotonotasdeclase.Fragmentos.GrupoFragment;
import dev.android.alexche77.fotonotasdeclase.Fragmentos.PerfilFragment;
import dev.android.alexche77.fotonotasdeclase.Fragmentos.PublicacionesFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        Fragment fragment = null;

        switch (position){
            case 0:
                fragment = PublicacionesFragment.newInstance();
                break;
            case 1:
                fragment = GrupoFragment.newInstance();
                break;
            case 2:
                fragment = PerfilFragment.newInstance();
                break;
            default:
                fragment = PublicacionesFragment.newInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "PUBLICACIONES";
            case 1:
                return "GRUPOS";
            case 2:
                return "PERFIL";
        }
        return null;
    }
}