package dev.android.alexche77.fotonotasdeclase.Modelos;

import com.github.thunder413.datetimeutils.DateTimeUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by alvaro on 9/29/17.
 */

public class Publicacion {
    public String getFechaPublicacionFormateada(){
        Date fecha = DateTimeUtils.formatDate(fechapublicacion);
        return fecha.toString();

    }
    public long getFechapublicacion() {
        return fechapublicacion;
    }

    public void setFechapublicacion(long fechapublicacion) {
        this.fechapublicacion = fechapublicacion;
    }

    public String getGrupoID() {
        return grupoID;
    }

    public void setGrupoID(String grupoID) {
        this.grupoID = grupoID;
    }

    public String getGrupoNombre() {
        return grupoNombre;
    }

    public void setGrupoNombre(String grupoNombre) {
        this.grupoNombre = grupoNombre;
    }

    public String getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(String usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getUsuarioNombre() {
        return usuarioNombre;
    }

    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }




    public String getFechaFormateada() {
        return fechaFormateada;
    }

    public void setFechaFormateada(String fechaFormateada) {
        this.fechaFormateada = fechaFormateada;
    }


    public Map<String, FotoNota> getFotonotas() {
        return fotonotas;
    }

    public void setFotonotas(Map<String, FotoNota> fotonotas) {
        this.fotonotas = fotonotas;
    }

    private Map<String,FotoNota> fotonotas;

    private long fechapublicacion;
    //    El id del grupo al que pertenece esta publicación
    private String grupoID;
    private String grupoNombre;
    private String fechaFormateada;
//    El que creo la publicación
    private String usuarioID;
    private String usuarioNombre;



}
