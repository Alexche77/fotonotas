package dev.android.alexche77.fotonotasdeclase.Fragmentos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import dev.android.alexche77.fotonotasdeclase.Modelos.Grupo;
import dev.android.alexche77.fotonotasdeclase.R;
import dev.android.alexche77.fotonotasdeclase.Utilidades.Constantes;
import dev.android.alexche77.fotonotasdeclase.Utilidades.RecyclerItemClickListener;
import dev.android.alexche77.fotonotasdeclase.Utilidades.ViewHolders.GrupoHolder;


public class GrupoFragment extends Fragment {


    private FirebaseRecyclerAdapter<Grupo, GrupoHolder> mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GrupoFragment() {
    }


    public static GrupoFragment newInstance() {

        return new GrupoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grupo_list, container, false);
//        Creamos la referencia al nodo de grupos
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(Constantes.Firebase.GRUPOS_NODO);
        ref.keepSynced(true);
//        FirebaseAuth auth = FirebaseAuth.getInstance();
//        Mandamos uno de pruebita

//        if (auth.getCurrentUser() !=null){
//            Grupo grupo = new Grupo();
//            grupo.setCreador(auth.getCurrentUser().getDisplayName());
//            grupo.setIdCreador(auth.getCurrentUser().getUid());
//            grupo.setNombre("4T1-Co");
//            ref.push().setValue(grupo).addOnCompleteListener(new OnCompleteListener<Void>() {
//                @Override
//                public void onComplete(@NonNull Task<Void> task) {
//                    Toast.makeText(getContext(),
//                            "Grupo nuevo agregada a la lista",
//                            Toast.LENGTH_LONG)
//                            .show();
//                }
//            });
//        }

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            mAdapter = new FirebaseRecyclerAdapter<Grupo, GrupoHolder>(
                    Grupo.class,
                    R.layout.fragment_grupo,
                    GrupoHolder.class,
                    ref) {
                @Override
                public void populateViewHolder(GrupoHolder holder, Grupo grupo, int position) {
                    holder.setNombreGrupo(grupo.getNombre());
                    holder.setCreadoPor(grupo.getCreador());
                }
            };
            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Snackbar.make(getActivity().findViewById(R.id.main_content),
                                    "Grupo: "+mAdapter.getItem(position).getNombre(),
                                    Snackbar.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                        }
                    })
            );

            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

        mAdapter.cleanup();

    }


}
