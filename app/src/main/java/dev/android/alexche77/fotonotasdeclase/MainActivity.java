package dev.android.alexche77.fotonotasdeclase;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import dev.android.alexche77.fotonotasdeclase.Fragmentos.PerfilFragment;
import dev.android.alexche77.fotonotasdeclase.Modelos.Grupo;
import dev.android.alexche77.fotonotasdeclase.Utilidades.Constantes;
import dev.android.alexche77.fotonotasdeclase.Utilidades.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity implements PerfilFragment.ListenerFragment{

    private static final int OK_CODE = 200;
    private static final int RETORNO_PUBLICACION = 500;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private FloatingActionButton fab;
    private CoordinatorLayout mRoot;
    private boolean tienePermisos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mRoot = (CoordinatorLayout) findViewById(R.id.main_content);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null) {
            iniciarLoginActivity();
            finish();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorHeight(20);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        if (!fab.isShown()) fab.show();
                        break;
                    case 1:
                        if (!fab.isShown()) fab.show();
                        break;
                    case 2:
                        fab.hide();
                    default:
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.setupWithViewPager(mViewPager);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("FAB", "onClick: Recibido" + mViewPager.getCurrentItem());
                switch (mViewPager.getCurrentItem()) {
                    case 0:
                        int rExtCheck = ContextCompat.checkSelfPermission(MainActivity.this,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE);
                        int wExtCheck = ContextCompat.checkSelfPermission(MainActivity.this,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        int cameraCheck = ContextCompat.checkSelfPermission(MainActivity.this,
                                android.Manifest.permission.CAMERA);

                        if (rExtCheck == PackageManager.PERMISSION_DENIED
                                || wExtCheck == PackageManager.PERMISSION_DENIED
                                || cameraCheck == PackageManager.PERMISSION_DENIED) {
//                            Significa que no tenemos los permisos!
//                            Entonces los pedimos
                            Log.e("MAIN", "Permisos: Hay permisos denegados");
                            // Should we show an explanation?
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                            android.Manifest.permission.CAMERA},
                                    OK_CODE);
                        } else {
                            Intent i = new Intent(MainActivity.this, CrearPublicacion.class);
                            startActivityForResult(i,RETORNO_PUBLICACION);
                        }


                        break;
                    case 1:
                        dialogoNuevoGrupo();

                        break;
                    default:
                        Snackbar.make(view, "Creando una nueva publicacion", Snackbar.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }


    private void iniciarLoginActivity() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void CerrarSesion() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {

                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                    }
                });
    }

    @Override
    public void LanzarActivityVideos() {
        Intent i = new Intent(MainActivity.this,VideoPlayer.class);
        startActivity(i);
    }


    private void dialogoNuevoGrupo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Nombre del nuevo grupo:");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String nombreGrupo = input.getText().toString();
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference(Constantes.Firebase.GRUPOS_NODO);
                FirebaseAuth auth = FirebaseAuth.getInstance();
                final Grupo grupo = new Grupo();
                grupo.setCreador(auth.getCurrentUser().getDisplayName());
                grupo.setIdCreador(auth.getCurrentUser().getUid());
                grupo.setNombre(nombreGrupo);
                ref.push().setValue(grupo).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Snackbar.make(mRoot, "Grupo agregado a tu lista:" + grupo.getNombre(), Snackbar.LENGTH_SHORT).show();

                    }
                });
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RETORNO_PUBLICACION) {

            if (resultCode == RESULT_OK) {
                Snackbar.make(mRoot, "Grupo agregado a tu lista", Snackbar.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == OK_CODE)
            tienePermisos = grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

}

