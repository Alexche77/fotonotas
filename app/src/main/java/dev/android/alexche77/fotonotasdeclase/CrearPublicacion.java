package dev.android.alexche77.fotonotasdeclase;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.android.alexche77.fotonotasdeclase.Modelos.FotoNota;
import dev.android.alexche77.fotonotasdeclase.Modelos.Grupo;
import dev.android.alexche77.fotonotasdeclase.Modelos.Publicacion;
import dev.android.alexche77.fotonotasdeclase.Utilidades.AdaptadorFotonotas;
import dev.android.alexche77.fotonotasdeclase.Utilidades.Constantes;

public class CrearPublicacion extends AppCompatActivity {

    private static final int OK_CODE = 200;
    private Spinner spinner;

    ArrayList<String> llaves;
    ArrayList<Grupo> grupos;
    List<Uri> mSelected;
    ArrayList<FotoNota> fotoNotas;
    Map<String, FotoNota> fotoNotasDB;
    private ArrayAdapter<Grupo> arrayAdapter;
    private RecyclerView rv;
    private int IMAGENES_SELECCIONADAS = 200;
    private AdaptadorFotonotas mAdapter;
    private StorageReference storageReference;
    private int subidas = 0;
    private Publicacion p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_publicacion);
        spinner = findViewById(R.id.spinner);
        rv = findViewById(R.id.recyclerView);

        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));

        llaves = new ArrayList<>();
        grupos = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<Grupo>(this, android.R.layout.simple_list_item_1, grupos);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);


//        Conectandonos a FBase
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constantes.Firebase.GRUPOS_NODO);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //iterating through all the values in database
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Grupo grupo = postSnapshot.getValue(Grupo.class);
                    Log.d("GRUPO", "Obtenido" + grupo.getNombre());
                    llaves.add(postSnapshot.getKey());
                    grupos.add(grupo);
                }
                arrayAdapter.notifyDataSetChanged();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Button btnAgregarFotos = findViewById(R.id.btnAgregarFotos);
        btnAgregarFotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Matisse.from(CrearPublicacion.this)
                        .choose(MimeType.allOf())
                        .countable(true)
                        .maxSelectable(9)
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .thumbnailScale(0.85f)
                        .imageEngine(new GlideEngine())
                        .forResult(IMAGENES_SELECCIONADAS);

            }
        });
        fotoNotas = new ArrayList<>();
        fotoNotasDB = new HashMap<String, FotoNota>();
        mAdapter = new AdaptadorFotonotas(getApplicationContext(), fotoNotas);
        rv.setAdapter(mAdapter);

        storageReference = FirebaseStorage.getInstance().getReference();
        Button btnPublicar = findViewById(R.id.btnPublicar);
        btnPublicar.setOnClickListener(new View.OnClickListener() {
            public String nuevaLlave;
            public DatabaseReference referenciaPublicaciones;

            @Override
            public void onClick(View view) {

                referenciaPublicaciones = FirebaseDatabase.getInstance().getReference(Constantes.Firebase.PUBLICACIONES_NODO);
                nuevaLlave = referenciaPublicaciones.push().getKey();

                p = new Publicacion();
                p.setUsuarioID(FirebaseAuth.getInstance().getCurrentUser().getUid());
                p.setUsuarioNombre(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                p.setGrupoNombre(grupos.get(spinner.getSelectedItemPosition()).getNombre());
                p.setGrupoID(llaves.get(spinner.getSelectedItemPosition()));
                p.setFechapublicacion(System.currentTimeMillis());

//              Vamos a subir estos archivos
                for (final FotoNota fotonota :
                        fotoNotas) {
//                    Deberia de guardar en DB/publicaciones/<idPublicacion>/<nombreUnico>.extension
                    StorageReference sRef = storageReference.child(Constantes.FirebaseStorage.PUBLICACIONES +
                            nuevaLlave + "/" +
                            fotonota.getNombreUnico());

//                    Una vez creada la referencia, hacemos uso de putFile
                    sRef.putFile(Uri.parse(fotonota.getUrl())).addOnSuccessListener(
                            new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                                    Ahora que hemos subido a storage las fotos
//                                    Falta referenciar dichas fotos en database y ponerles sus url a la
//                                    publicacion

                                    subidas++;
                                    FotoNota ft = new FotoNota();
                                    ft.setNombreUnico(fotonota.getNombreUnico());
                                    ft.setUrl(taskSnapshot.getDownloadUrl().toString());
//                                    Agregamos nuestra fotonota nueva con su url en FBSTORAGE
//                                    a nuestro arraylist auxiliar, que asignaremos a la publicacion
//                                    y pushearemos a firebase
//                                    fotoNotasDB.add(ft);
                                    fotoNotasDB.put("ft"+String.valueOf(subidas), ft);
                                    Log.d("MAP", "Agregamos al mapa->" + fotoNotasDB.size());
                                    if (subidas == fotoNotas.size()) {
                                        p.setFotonotas(fotoNotasDB);
                                        referenciaPublicaciones.child(nuevaLlave).setValue(p).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                finish();
                                            }
                                        });
                                    }

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            Log.d("SUBIENDO", "Se han subido " + ((int) progress) + "% de " + subidas + "/" + fotoNotas.size());
                        }
                    });
                }
//                En este punto la subida debio haber sido exitosa
//                Creamos el post en la base de datos


//

                Toast.makeText(getApplicationContext(), "Crecion finalizada, sal de esta pantalla", Toast.LENGTH_SHORT).show();


            }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGENES_SELECCIONADAS && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data);
            Log.d("Matisse", "mSelected: " + mSelected);
            for (Uri uri : mSelected
                    ) {
                Log.d("IMAGENES", "URI" + uri.toString());
                FotoNota foto = new FotoNota();
                foto.setUrl(uri.toString());
                foto.setNombreUnico(System.currentTimeMillis() + "." + obtenerExtension(uri));
                fotoNotas.add(foto);
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    public String obtenerExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
}
