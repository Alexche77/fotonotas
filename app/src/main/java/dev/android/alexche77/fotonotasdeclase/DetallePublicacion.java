package dev.android.alexche77.fotonotasdeclase;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import dev.android.alexche77.fotonotasdeclase.Modelos.FotoNota;
import dev.android.alexche77.fotonotasdeclase.Modelos.Publicacion;
import dev.android.alexche77.fotonotasdeclase.Utilidades.Constantes;

public class DetallePublicacion extends AppCompatActivity {
    private static final String TAG = DetallePublicacion.class.getSimpleName();
    private String llavePost;
    private RecyclerView recyclerView;
    private TextView nombreOP, grupoNombre;
    private ArrayList<FotoNota> fotonotas;
    private FotoNotaAdapter fnAdapter;
    private Button btnGuardado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_publicacion);
        recyclerView = findViewById(R.id.list);
        nombreOP = findViewById(R.id.publicadaPor);
        grupoNombre = findViewById(R.id.grupoPublicada);
        fotonotas = new ArrayList<>();
        fnAdapter = new FotoNotaAdapter(fotonotas);
        btnGuardado = findViewById(R.id.btnGuardar);
        llavePost = getIntent().getStringExtra("postKey");

        Log.d(TAG, "onViewCreated: Recibimos la llave->"+llavePost);
        DatabaseReference refPost = FirebaseDatabase.getInstance().getReference(Constantes.Firebase.PUBLICACIONES_NODO);
        refPost.child(llavePost).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange: Recibimos un snapshot de firebase");
                Publicacion p = dataSnapshot.getValue(Publicacion.class);
                if(p!=null){
                    Log.d(TAG, "onDataChange: Parseado correctamente el post publicado en el grupo"+p.getGrupoNombre());
                    for (FotoNota f :
                            p.getFotonotas().values()) {
                        Log.d(TAG, "Fotonotas->"+f.getUrl());
                        fotonotas.add(f);
                    }
                    fnAdapter.notifyDataSetChanged();
                    if (nombreOP != null)
                        nombreOP.setText("Publicado por: "+p.getUsuarioNombre());
                    if (grupoNombre !=null)
                        grupoNombre.setText("En el grupo: "+p.getGrupoNombre());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));
        recyclerView.setAdapter(fnAdapter);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView fotoNota;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.fragment_fotonota_list_dialog_item, parent, false));
            fotoNota = itemView.findViewById(R.id.imgView);

        }

    }

    private class FotoNotaAdapter extends RecyclerView.Adapter<ViewHolder> {

        private ArrayList<FotoNota> fotoNotasMap;

        FotoNotaAdapter(ArrayList<FotoNota> fotoNotaMap) {
            fotoNotasMap = fotoNotaMap;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Glide.with(getApplicationContext())
                    .load(fotoNotasMap.get(position).getUrl())
                    .into(holder.fotoNota);
        }

        @Override
        public int getItemCount() {
            return fotoNotasMap.size();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void guardar(View view){
        Log.d(TAG, "guardar: Guardando las fotos");
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final long ONE_MEGABYTE = 1024 * 1024;
        for (FotoNota f :
                fotonotas) {
            Log.d(TAG, "guardar: Obteniendo el url..");
            StorageReference httpsReference = storage.getReferenceFromUrl(f.getUrl());
            try {
                final File localFile = File.createTempFile(f.getNombreUnico(),obtenerExtension(Uri.parse(f.getUrl())));
                httpsReference.getFile(localFile).addOnCompleteListener(new OnCompleteListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<FileDownloadTask.TaskSnapshot> task) {
                        Log.d(TAG, "onComplete: Se transfirieron "+task.getResult().getBytesTransferred()+" bytes..");
                        Log.d(TAG, "onComplete: Archivo generado en"+localFile.getPath());
                        btnGuardado.setText("Guardado en temportal");
                        btnGuardado.setEnabled(false);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                btnGuardado.setText("GUARDAR");
                btnGuardado.setEnabled(false);
            }


        }
    }

    public String obtenerExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
}
