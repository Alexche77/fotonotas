package dev.android.alexche77.fotonotasdeclase;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import dev.android.alexche77.fotonotasdeclase.Utilidades.Constantes;

public class VideoPlayer extends AppCompatActivity {
    private static final String TAG = VideoPlayer.class.getSimpleName();
    Button btnAnt, btnSig, btnGuardar;
    private VideoView videoPlayer;
    private ProgressBar pb;
    private StorageReference refVideos;
    private File videosGuardados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        pb = findViewById(R.id.progressBar2);
        pb.isIndeterminate();
        btnSig = findViewById(R.id.button2);
        btnAnt = findViewById(R.id.button3);
        btnGuardar = findViewById(R.id.button4);

        videoPlayer = findViewById(R.id.videoFB);
        refVideos = FirebaseStorage.getInstance().getReference("videos");

//        El primer paso es verificar mi estructura interna, si tengo el archivo.
//        Para eso creamos la estructura /videos donde guardaremos todo el asunto
        videosGuardados = new File(getFilesDir()+"/videosGuardados","vid.mp4");

            if (videosGuardados.mkdir()){
                Log.d(TAG, "onCreate: Se creo la estructura correctamente, verificando si hay contenido"+videosGuardados.toString());
            }else{
                Log.d(TAG, "onCreate: Hubo un problema"+videosGuardados.toString());

            }
            if (videosGuardados.isFile()){
                btnGuardar.setEnabled(false);
                videoPlayer.setVideoURI(Uri.parse(videosGuardados.getPath()));
                videoPlayer.start();
            }else{
                btnGuardar.setEnabled(true);
                Log.d(TAG, "onCreate: VideosGuardados es un directorio");
                        refVideos.child("vid.mp4").getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                Uri uri = task.getResult();
                                Log.d(TAG, "onCreate: Hemos cargado el URI del video"+uri);
                                if (videoPlayer.isPlaying()){
                                    videoPlayer.stopPlayback();
                                }else{
                                    videoPlayer.setVideoURI(uri);
                                    videoPlayer.start();
                                }

                            }
                        });


        }




        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videosGuardados.isFile())
                    Log.d(TAG, "onClick: Tratando de guardar en"+videosGuardados.getPath());
                else
                    Log.d(TAG, "onClick: Se quiere gurdar un directorio"+videosGuardados.getPath());
                refVideos.child("vid.mp4").getFile(videosGuardados).addOnCompleteListener(new OnCompleteListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<FileDownloadTask.TaskSnapshot> task) {
                        pb.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(),"Video guardado en "+task.getResult(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });




    }


}
