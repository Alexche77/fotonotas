package dev.android.alexche77.fotonotasdeclase.Fragmentos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import dev.android.alexche77.fotonotasdeclase.DetallePublicacion;
import dev.android.alexche77.fotonotasdeclase.MainActivity;
import dev.android.alexche77.fotonotasdeclase.Modelos.Publicacion;
import dev.android.alexche77.fotonotasdeclase.R;
import dev.android.alexche77.fotonotasdeclase.Utilidades.Constantes;
import dev.android.alexche77.fotonotasdeclase.Utilidades.RecyclerItemClickListener;
import dev.android.alexche77.fotonotasdeclase.Utilidades.ViewHolders.PublicacionHolder;


public class PublicacionesFragment extends Fragment {


    private FirebaseRecyclerAdapter<Publicacion, PublicacionHolder> mAdapter;


    public PublicacionesFragment() {
    }

    public static PublicacionesFragment newInstance() {
        return new PublicacionesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publicacion_list, container, false);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(Constantes.Firebase.PUBLICACIONES_NODO);
//        Con esta linea habilitamos la persistencia de datosy que se mantenga sincronizada
        ref.keepSynced(true);
//        FirebaseAuth auth = FirebaseAuth.getInstance();
//        Mandamos uno de pruebita
//        Publicacion publicacion = new Publicacion();
//        publicacion.setFechapublicacion(new Date().getTime());
//        publicacion.setGrupoNombre("4T1-CO");
//        publicacion.setGrupoID("1");
//        if (auth.getCurrentUser() !=null){
//            publicacion.setUsuarioNombre(auth.getCurrentUser().getDisplayName());
//            publicacion.setUsuarioID(auth.getCurrentUser().getUid());
//        }
//        ref.push().setValue(publicacion).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//                Toast.makeText(getContext(),
//                        "Publicacion ha sido agregada a la lista",
//                        Toast.LENGTH_LONG)
//                        .show();
//            }
//        });
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            mAdapter = new FirebaseRecyclerAdapter<Publicacion, PublicacionHolder>(
                    Publicacion.class,
                    R.layout.fragment_publicacion,
                    PublicacionHolder.class,
                    ref) {
                @Override
                public void populateViewHolder(PublicacionHolder holder, Publicacion publicacion, int position) {
                    holder.setNombreGrupo(publicacion.getGrupoNombre());
                    holder.setFecha(publicacion.getFechaPublicacionFormateada());
                }
            };
            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Snackbar.make(getActivity().findViewById(R.id.main_content),
                                    "Publicacion de "+mAdapter.getItem(position).getUsuarioNombre(),
                                    Snackbar.LENGTH_SHORT).show();

//                            FotoNotaListDialogFragment f = FotoNotaListDialogFragment.newInstance(mAdapter.getRef(position).getKey());

                            Intent i = new Intent(getActivity(), DetallePublicacion.class);
                            i.putExtra("postKey", mAdapter.getRef(position).getKey());
                            startActivity(i);


                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                        }
                    })
            );

            recyclerView.setAdapter(mAdapter);

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

        mAdapter.cleanup();
    }


    public interface interaccionFotoNota {
        // TODO: Update argument type and name
        void interaccionFotoNotas(Publicacion item);
    }
}
