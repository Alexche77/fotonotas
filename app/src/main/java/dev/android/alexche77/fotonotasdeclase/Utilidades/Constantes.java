package dev.android.alexche77.fotonotasdeclase.Utilidades;

/**
 * Created by alvaro on 9/29/17.
 */

public class Constantes {
    public class Firebase {

        public static final String PUBLICACIONES_NODO = "publicaciones";
        public static final String GRUPOS_NODO = "grupos";
    }

    public class FirebaseStorage {
        public static final String PUBLICACIONES = "publicaciones/";
    }
}
