package dev.android.alexche77.fotonotasdeclase.Utilidades.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import dev.android.alexche77.fotonotasdeclase.R;

/**
 * Created by alvaro on 9/29/17.
 */

public class GrupoHolder extends RecyclerView.ViewHolder {
    private final TextView nombreGrupo;
    private final TextView creadoPor;
    public GrupoHolder(View itemView) {
        super(itemView);
        nombreGrupo = itemView.findViewById(R.id.grupoNombre);
        creadoPor = itemView.findViewById(R.id.creadorGrupo);
    }

    public void setCreadoPor(String creador) {
        creadoPor.setText(creador);
    }

    public void setNombreGrupo(String nombre) {
        nombreGrupo.setText(nombre);
    }
}
