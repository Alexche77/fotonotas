package dev.android.alexche77.fotonotasdeclase.Utilidades.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import dev.android.alexche77.fotonotasdeclase.R;

/**
 * Created by alvaro on 9/29/17.
 */

public class PublicacionHolder extends RecyclerView.ViewHolder {


    private final TextView fecha;
    private final TextView nombreGrupo;

    public PublicacionHolder(View itemView) {
        super(itemView);
        fecha = itemView.findViewById(R.id.fechaGrupo);
        nombreGrupo = itemView.findViewById(R.id.nombreGrupo);
    }

    public void setNombreGrupo(String nombre){
        nombreGrupo.setText(nombre);
    }

    public void setFecha(String f){
        fecha.setText(f);
    }

}
